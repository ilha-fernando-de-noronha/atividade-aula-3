# Atividade aula 3

## Version 0.1.0-alpha

Inicio do projeto (html)

## Version 0.2.0-alpha

Adicionado inputs e operação de adição ao html

## Version 0.3.0-beta

Adicionado operação de subtração ao html

## Version 0.4.0-beta

Adicionado biblioteca externa TailwindCss

## Version 1.0.0

Lançamento Oficial;
Adicionado funções de adição e subtração ao script

## Version 1.0.1

Corrigido erro devido a falta do resultado aparecer na tela

## Version 1.1.0

Alterado para modo escuro

## Version 1.2.0

Adicionado footer com créditos à equipe

## Version 1.2.1

Ajustada url de créditos

## Version 2.0.0

Adicionado funções de multiplicação e divisão