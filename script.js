function calculate () {
    var num1 = Number(document.getElementById('first_num').value);
    var num2 = Number(document.getElementById('second_num').value);
    var operator = document.getElementById('operation').value;
    var result = document.getElementById('result');
    switch(operator) {
        case '+':
            result.innerText = num1 + num2;
            break;
        case '-':
            result.innerText = num1 - num2;
            break;
        case '*':
            result.innerText = (num1 * num2).toFixed(2);
            break;
        case '/':
            result.innerText = (num1 / num2).toFixed(2);
            break;
    }
}